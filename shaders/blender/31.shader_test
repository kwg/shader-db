[require]
GLSL >= 4.30

[vertex shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_VERTEX_SHADER
#define USE_GPU_SHADER_CREATE_INFO

/* Pass Resources. */
layout(binding = 0) uniform sampler2D image;

/* Batch Resources. */

/* Push Constants. */
uniform vec4 color;
uniform float factor;
uniform mat4 ModelViewProjectionMatrix;


/* Inputs. */
layout(location = 0) in vec2 pos;
layout(location = 1) in vec2 texCoord;

/* Interfaces. */
out smooth_tex_coord_interp_iface{
  smooth vec2 texCoord_interp;
};

#ifndef USE_GPU_SHADER_CREATE_INFO
uniform mat4 ModelViewProjectionMatrix;

/* Keep in sync with intern/opencolorio/gpu_shader_display_transform_vertex.glsl */
in vec2 texCoord;
in vec2 pos;
out vec2 texCoord_interp;
#endif

void main()
{
  gl_Position = ModelViewProjectionMatrix * vec4(pos.xy, 0.0f, 1.0f);
  gl_Position.z = 1.0;
  texCoord_interp = texCoord;
}

[fragment shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_FRAGMENT_SHADER
#define USE_GPU_SHADER_CREATE_INFO

/* Pass Resources. */
layout(binding = 0) uniform sampler2D image;

/* Batch Resources. */

/* Push Constants. */
uniform vec4 color;
uniform float factor;
uniform mat4 ModelViewProjectionMatrix;


/* Interfaces. */
in smooth_tex_coord_interp_iface{
  smooth vec2 texCoord_interp;
};
layout(depth_any) out float gl_FragDepth;

/* Outputs. */
layout(location = 0) out vec4 fragColor;


void main()
{
  vec4 tex = texture(image, texCoord_interp);
  tex.rgb = ((0.3333333 * factor) * vec3(tex.r + tex.g + tex.b)) + (tex.rgb * (1.0 - factor));
  fragColor = tex * color;
}

