[require]
GLSL >= 4.30

[vertex shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_VERTEX_SHADER
#define USE_GPU_SHADER_CREATE_INFO

/* Pass Resources. */

/* Batch Resources. */

/* Push Constants. */
uniform mat4 ModelViewProjectionMatrix;
uniform vec2 viewport_size;
uniform float dash_width;
uniform float dash_factor;
uniform int colors_len;
uniform vec4 color;
uniform vec4 color2;


/* Inputs. */
layout(location = 0) in vec3 pos;

/* Interfaces. */
out flat_color_iface{
  flat vec4 finalColor;
};
out gpu_shader_line_dashed_interface{
  noperspective vec2 stipple_start;
  flat vec2 stipple_pos;
};

#ifdef USE_WORLD_CLIP_PLANES
#  if defined(GPU_VERTEX_SHADER) || defined(GPU_GEOMETRY_SHADER)

#    ifndef USE_GPU_SHADER_CREATE_INFO
uniform vec4 WorldClipPlanes[6];
#    endif

#    define _world_clip_planes_calc_clip_distance(wpos, _clipplanes) \
      { \
        vec4 _pos = vec4(wpos, 1.0); \
        gl_ClipDistance[0] = dot(_clipplanes[0], _pos); \
        gl_ClipDistance[1] = dot(_clipplanes[1], _pos); \
        gl_ClipDistance[2] = dot(_clipplanes[2], _pos); \
        gl_ClipDistance[3] = dot(_clipplanes[3], _pos); \
        gl_ClipDistance[4] = dot(_clipplanes[4], _pos); \
        gl_ClipDistance[5] = dot(_clipplanes[5], _pos); \
      }

/* When all shaders are builtin shaders are migrated this could be applied directly. */
#    ifdef USE_GPU_SHADER_CREATE_INFO
#      define WorldClipPlanes clipPlanes.world
#    endif
/* HACK Dirty hack to be able to override the definition in common_view_lib.glsl.
 * Not doing this would require changing the include order in every shaders. */
#    define world_clip_planes_calc_clip_distance(wpos) \
      _world_clip_planes_calc_clip_distance(wpos, WorldClipPlanes)

#  endif

#  define world_clip_planes_set_clip_distance(c) \
    { \
      gl_ClipDistance[0] = (c)[0]; \
      gl_ClipDistance[1] = (c)[1]; \
      gl_ClipDistance[2] = (c)[2]; \
      gl_ClipDistance[3] = (c)[3]; \
      gl_ClipDistance[4] = (c)[4]; \
      gl_ClipDistance[5] = (c)[5]; \
    }

#endif
#pragma BLENDER_REQUIRE(gpu_shader_cfg_world_clip_lib.glsl)

/*
 * Vertex Shader for dashed lines with 3D coordinates,
 * with uniform multi-colors or uniform single-color, and unary thickness.
 *
 * Dashed is performed in screen space.
 */

void main()
{
  vec4 pos_4d = vec4(pos, 1.0);
  gl_Position = ModelViewProjectionMatrix * pos_4d;
  stipple_start = stipple_pos = viewport_size * 0.5 * (gl_Position.xy / gl_Position.w);
#ifdef USE_WORLD_CLIP_PLANES
  world_clip_planes_calc_clip_distance((ModelMatrix * pos_4d).xyz);
#endif
}

[fragment shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_FRAGMENT_SHADER
#define USE_GPU_SHADER_CREATE_INFO

/* Pass Resources. */

/* Batch Resources. */

/* Push Constants. */
uniform mat4 ModelViewProjectionMatrix;
uniform vec2 viewport_size;
uniform float dash_width;
uniform float dash_factor;
uniform int colors_len;
uniform vec4 color;
uniform vec4 color2;


/* Interfaces. */
in flat_color_iface{
  flat vec4 finalColor;
};
in gpu_shader_line_dashed_interface{
  noperspective vec2 stipple_start;
  flat vec2 stipple_pos;
};
layout(depth_any) out float gl_FragDepth;

/* Outputs. */
layout(location = 0) out vec4 fragColor;


/*
 * Fragment Shader for dashed lines, with uniform multi-color(s),
 * or any single-color, and any thickness.
 *
 * Dashed is performed in screen space.
 */

void main()
{
  float distance_along_line = distance(stipple_pos, stipple_start);
  /* Solid line case, simple. */
  if (dash_factor >= 1.0f) {
    fragColor = color;
  }
  /* Actually dashed line... */
  else {
    float normalized_distance = fract(distance_along_line / dash_width);
    if (normalized_distance <= dash_factor) {
      fragColor = color;
    }
    else if (colors_len > 0) {
      fragColor = color2;
    }
    else {
      discard;
    }
  }
}

