[require]
GLSL >= 4.30

[vertex shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_VERTEX_SHADER
#define widgetID 0
#define MAX_PARAM 12
#define blender_srgb_to_framebuffer_space(a) a 
#define USE_GPU_SHADER_CREATE_INFO

/* Pass Resources. */

/* Batch Resources. */

/* Push Constants. */
uniform vec4 parameters[12];
uniform mat4 ModelViewProjectionMatrix;
uniform vec3 checkerColorAndSize;
uniform bool srgbTarget;


/* Inputs. */

/* Interfaces. */
out gpu_widget_iface{
  flat float discardFac;
  flat float lineWidth;
  flat vec2 outRectSize;
  flat vec4 borderColor;
  flat vec4 embossColor;
  flat vec4 outRoundCorners;
  noperspective float butCo;
  noperspective vec2 uvInterp;
  noperspective vec4 innerColor;
};

#define recti parameters[widgetID * MAX_PARAM + 0]
#define rect parameters[widgetID * MAX_PARAM + 1]
#define radsi parameters[widgetID * MAX_PARAM + 2].x
#define rads parameters[widgetID * MAX_PARAM + 2].y
#define faci parameters[widgetID * MAX_PARAM + 2].zw
#define roundCorners parameters[widgetID * MAX_PARAM + 3]
#define colorInner1 parameters[widgetID * MAX_PARAM + 4]
#define colorInner2 parameters[widgetID * MAX_PARAM + 5]
#define colorEdge parameters[widgetID * MAX_PARAM + 6]
#define colorEmboss parameters[widgetID * MAX_PARAM + 7]
#define colorTria parameters[widgetID * MAX_PARAM + 8]
#define tria1Center parameters[widgetID * MAX_PARAM + 9].xy
#define tria2Center parameters[widgetID * MAX_PARAM + 9].zw
#define tria1Size parameters[widgetID * MAX_PARAM + 10].x
#define tria2Size parameters[widgetID * MAX_PARAM + 10].y
#define shadeDir parameters[widgetID * MAX_PARAM + 10].z
#define alphaDiscard parameters[widgetID * MAX_PARAM + 10].w
#define triaType parameters[widgetID * MAX_PARAM + 11].x

/* We encode alpha check and discard factor together. */
#define doAlphaCheck (alphaDiscard < 0.0)
#define discardFactor abs(alphaDiscard)

vec2 do_widget(void)
{
  /* Offset to avoid losing pixels (mimics conservative rasterization). */
  const vec2 ofs = vec2(0.5, -0.5);
  lineWidth = abs(rect.x - recti.x);
  vec2 emboss_ofs = vec2(0.0, -lineWidth);

  vec2 pos;
  switch (gl_VertexID) {
    default:
    case 0: {
      pos = rect.xz + emboss_ofs + ofs.yy;
      break;
    }
    case 1: {
      pos = rect.xw + ofs.yx;
      break;
    }
    case 2: {
      pos = rect.yz + emboss_ofs + ofs.xy;
      break;
    }
    case 3: {
      pos = rect.yw + ofs.xx;
      break;
    }
  }

  uvInterp = pos - rect.xz;
  outRectSize = rect.yw - rect.xz;
  outRoundCorners = rads * roundCorners;

  vec2 uv = uvInterp / outRectSize;
  float fac = clamp((shadeDir > 0.0) ? uv.y : uv.x, 0.0, 1.0);
  /* Note innerColor is premultiplied inside the fragment shader. */
  if (doAlphaCheck) {
    innerColor = colorInner1;
    butCo = uv.x;
  }
  else {
    innerColor = mix(colorInner2, colorInner1, fac);
    butCo = -abs(uv.x);
  }

  /* We need premultiplied color for transparency. */
  borderColor = colorEdge * vec4(colorEdge.aaa, 1.0);
  embossColor = colorEmboss * vec4(colorEmboss.aaa, 1.0);

  return pos;
}

vec2 do_tria()
{
  int vidx = gl_VertexID % 4;
  bool tria2 = gl_VertexID > 7;

  vec2 pos;
  float size = (tria2) ? -tria2Size : tria1Size;
  vec2 center = (tria2) ? tria2Center : tria1Center;

  vec2 arrow_pos[4] = vec2[4](vec2(0.0, 0.6), vec2(0.6, 0.0), vec2(-0.6, 0.0), vec2(0.0, -0.6));
  /* Rotated uv space by 45deg and mirrored. */
  vec2 arrow_uvs[4] = vec2[4](vec2(0.0, 0.85), vec2(0.85, 0.85), vec2(0.0, 0.0), vec2(0.0, 0.85));

  vec2 point_pos[4] = vec2[4](vec2(-1.0, -1.0), vec2(-1.0, 1.0), vec2(1.0, -1.0), vec2(1.0, 1.0));
  vec2 point_uvs[4] = vec2[4](vec2(0.0, 0.0), vec2(0.0, 1.0), vec2(1.0, 0.0), vec2(1.0, 1.0));

  /* We reuse the SDF roundbox rendering of widget to render the tria shapes.
   * This means we do clever tricks to position the rectangle the way we want using
   * the 2 triangles uvs. */
  if (triaType == 0.0) {
    /* ROUNDBOX_TRIA_NONE */
    outRectSize = uvInterp = pos = vec2(0);
    outRoundCorners = vec4(0.01);
  }
  else if (triaType == 1.0) {
    /* ROUNDBOX_TRIA_ARROWS */
    pos = arrow_pos[vidx];
    uvInterp = arrow_uvs[vidx];
    uvInterp -= vec2(0.05, 0.63); /* Translate */
    outRectSize = vec2(0.74, 0.17);
    outRoundCorners = vec4(0.08);
  }
  else if (triaType == 2.0) {
    /* ROUNDBOX_TRIA_SCROLL */
    pos = point_pos[vidx];
    uvInterp = point_uvs[vidx];
    outRectSize = vec2(1.0);
    outRoundCorners = vec4(0.5);
  }
  else if (triaType == 3.0) {
    /* ROUNDBOX_TRIA_MENU */
    pos = tria2 ? vec2(0.0) : arrow_pos[vidx]; /* Solo tria */
    pos = vec2(pos.y, -pos.x);                 /* Rotate */
    pos += vec2(-0.05, 0.0);                   /* Translate */
    size *= 0.8;                               /* Scale */
    uvInterp = arrow_uvs[vidx];
    uvInterp -= vec2(0.05, 0.63); /* Translate */
    outRectSize = vec2(0.74, 0.17);
    outRoundCorners = vec4(0.01);
  }
  else if (triaType == 4.0) {
    /* ROUNDBOX_TRIA_CHECK */
    /* A bit more hacky: We use the two trias joined together to render
     * both sides of the checkmark with different length. */
    pos = arrow_pos[min(vidx, 2)];                                    /* Only keep 1 triangle. */
    pos.y = tria2 ? -pos.y : pos.y;                                   /* Mirror along X */
    pos = pos.x * vec2(0.0872, -0.996) + pos.y * vec2(0.996, 0.0872); /* Rotate (85deg) */
    pos += vec2(-0.1, 0.2);                                           /* Translate */
    center = tria1Center;
    size = tria1Size * 1.7; /* Scale */
    uvInterp = arrow_uvs[vidx];
    uvInterp -= tria2 ? vec2(0.4, 0.65) : vec2(0.08, 0.65); /* Translate */
    outRectSize = vec2(0.74, 0.14);
    outRoundCorners = vec4(0.01);
  }
  else {
    /* ROUNDBOX_TRIA_HOLD_ACTION_ARROW */
    /* We use a single triangle to cut the round rect in half. The edge will not be Antialiased. */
    pos = tria2 ? vec2(0.0) : arrow_pos[min(vidx, 2)];              /* Only keep 1 triangle. */
    pos = pos.x * vec2(0.707, 0.707) + pos.y * vec2(-0.707, 0.707); /* Rotate (45deg) */
    pos += vec2(-1.7, 2.4); /* Translate  (hardcoded, might want to remove) */
    size *= 0.4;            /* Scale */
    uvInterp = arrow_uvs[vidx];
    uvInterp -= vec2(0.05, 0.05); /* Translate */
    outRectSize = vec2(0.75);
    outRoundCorners = vec4(0.01);
  }

  uvInterp *= abs(size);
  outRectSize *= abs(size);
  outRoundCorners *= abs(size);

  pos = pos * size + center;

  innerColor = colorTria * vec4(colorTria.aaa, 1.0);

  lineWidth = 0.0;
  borderColor = vec4(0.0);
  embossColor = vec4(0.0);

  butCo = -2.0;

  return pos;
}

void main()
{
  discardFac = discardFactor;
  bool is_tria = (gl_VertexID > 3);
  vec2 pos = (is_tria) ? do_tria() : do_widget();

  gl_Position = ModelViewProjectionMatrix * vec4(pos, 0.0, 1.0);
}

[fragment shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_FRAGMENT_SHADER
#define widgetID 0
#define MAX_PARAM 12
#define blender_srgb_to_framebuffer_space(a) a 
#define USE_GPU_SHADER_CREATE_INFO

/* Pass Resources. */

/* Batch Resources. */

/* Push Constants. */
uniform vec4 parameters[12];
uniform mat4 ModelViewProjectionMatrix;
uniform vec3 checkerColorAndSize;
uniform bool srgbTarget;


/* Interfaces. */
in gpu_widget_iface{
  flat float discardFac;
  flat float lineWidth;
  flat vec2 outRectSize;
  flat vec4 borderColor;
  flat vec4 embossColor;
  flat vec4 outRoundCorners;
  noperspective float butCo;
  noperspective vec2 uvInterp;
  noperspective vec4 innerColor;
};
layout(depth_any) out float gl_FragDepth;

/* Outputs. */
layout(location = 0) out vec4 fragColor;


/* Undefine the macro that avoids compilation errors. */
#undef blender_srgb_to_framebuffer_space

#ifndef USE_GPU_SHADER_CREATE_INFO
uniform bool srgbTarget = false;
#endif

vec4 blender_srgb_to_framebuffer_space(vec4 in_color)
{
  if (srgbTarget) {
    vec3 c = max(in_color.rgb, vec3(0.0));
    vec3 c1 = c * (1.0 / 12.92);
    vec3 c2 = pow((c + 0.055) * (1.0 / 1.055), vec3(2.4));
    in_color.rgb = mix(c1, c2, step(vec3(0.04045), c));
  }
  return in_color;
}
#pragma BLENDER_REQUIRE(gpu_shader_colorspace_lib.glsl)

vec3 compute_masks(vec2 uv)
{
  bool upper_half = uv.y > outRectSize.y * 0.5;
  bool right_half = uv.x > outRectSize.x * 0.5;
  float corner_rad;

  /* Correct aspect ratio for 2D views not using uniform scalling.
   * uv is already in pixel space so a uniform scale should give us a ratio of 1. */
  float ratio = (butCo != -2.0) ? (dFdy(uv.y) / dFdx(uv.x)) : 1.0;
  vec2 uv_sdf = uv;
  uv_sdf.x *= ratio;

  if (right_half) {
    uv_sdf.x = outRectSize.x * ratio - uv_sdf.x;
  }
  if (upper_half) {
    uv_sdf.y = outRectSize.y - uv_sdf.y;
    corner_rad = right_half ? outRoundCorners.z : outRoundCorners.w;
  }
  else {
    corner_rad = right_half ? outRoundCorners.y : outRoundCorners.x;
  }

  /* Fade emboss at the border. */
  float emboss_size = upper_half ? 0.0 : min(1.0, uv_sdf.x / (corner_rad * ratio));

  /* Signed distance field from the corner (in pixel).
   * inner_sdf is sharp and outer_sdf is rounded. */
  uv_sdf -= corner_rad;
  float inner_sdf = max(0.0, min(uv_sdf.x, uv_sdf.y));
  float outer_sdf = -length(min(uv_sdf, 0.0));
  float sdf = inner_sdf + outer_sdf + corner_rad;

  /* Clamp line width to be at least 1px wide. This can happen if the projection matrix
   * has been scaled (i.e: Node editor)... */
  float line_width = (lineWidth > 0.0) ? max(fwidth(uv.y), lineWidth) : 0.0;

  const float aa_radius = 0.5;
  vec3 masks;
  masks.x = smoothstep(-aa_radius, aa_radius, sdf);
  masks.y = smoothstep(-aa_radius, aa_radius, sdf - line_width);
  masks.z = smoothstep(-aa_radius, aa_radius, sdf + line_width * emboss_size);

  /* Compose masks together to avoid having too much alpha. */
  masks.zx = max(vec2(0.0), masks.zx - masks.xy);

  return masks;
}

vec4 do_checkerboard()
{
  float size = checkerColorAndSize.z;
  vec2 phase = mod(gl_FragCoord.xy, size * 2.0);

  if ((phase.x > size && phase.y < size) || (phase.x < size && phase.y > size)) {
    return vec4(checkerColorAndSize.xxx, 1.0);
  }
  else {
    return vec4(checkerColorAndSize.yyy, 1.0);
  }
}

void main()
{
  if (min(1.0, -butCo) > discardFac) {
    discard;
  }

  vec3 masks = compute_masks(uvInterp);

  if (butCo > 0.0) {
    /* Alpha checker widget. */
    if (butCo > 0.5) {
      vec4 checker = do_checkerboard();
      fragColor = mix(checker, innerColor, innerColor.a);
    }
    else {
      /* Set alpha to 1.0. */
      fragColor = innerColor;
    }
    fragColor.a = 1.0;
  }
  else {
    /* Premultiply here. */
    fragColor = innerColor * vec4(innerColor.aaa, 1.0);
  }
  fragColor *= masks.y;
  fragColor += masks.x * borderColor;
  fragColor += masks.z * embossColor;

  /* Un-premult because the blend equation is already doing the mult. */
  if (fragColor.a > 0.0) {
    fragColor.rgb /= fragColor.a;
  }

  fragColor = blender_srgb_to_framebuffer_space(fragColor);
}

